import { Component }   from '@angular/core';
import { Router }      from '@angular/router';
import { AuthService } from './services/auth.service';
import { HeaderTitleService } from './services/header-title.service';
  import { from } from 'rxjs';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css'],
})
export class AppComponent {
  title = '';
  
  constructor(private router:Router, public auth:AuthService, private headerTitleService: HeaderTitleService){

    auth.getUser();
    if(!auth.user){
      this.router.navigateByUrl('/');
    }
  }

  ngOnInit() {
    this.headerTitleService.title.subscribe(title => {
      this.title = title;
    })
  }
}
