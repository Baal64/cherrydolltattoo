import { Component, OnInit }         from '@angular/core';
import { ClientService }             from '../../services/client.service';
import { ListeClient }               from '../liste-clients/liste-clients.interface';
import { Router }                    from '@angular/router';
import { OnDisableComponentService } from '../../services/on-disable-component.service';
import { HeaderTitleService }        from 'src/app/services/header-title.service';

@Component({
  selector   : 'app-identification-client',
  templateUrl: './identification-client.component.html',
  styleUrls  : ['./identification-client.component.css']
})

export class IdentificationClientComponent implements OnInit {
  public shown = false;

  customers: ListeClient = {
    id_client : null,
    nom       : '',
    prenom    : '',
    adresse1  : '',
    adresse2  : '',
    ville     : '',
    codePostal: null,
    mail      : '',
    tel       : null,
    date      : null
  };

  public constructor( private clientService            : ClientService,
                      private router                   : Router,
                      private onDisableComponentService: OnDisableComponentService,
                      private headerTitleService       : HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Nouveau client')
  }
  
  // Method from service to disable the checkbox after the click
  // and resize the article
  onDisableComponent() {
    this.onDisableComponentService.onDisableComponent();
  }

  // Methods to send data to service "clientservice"
  createCustomer(data: ListeClient) {
    this.clientService.createCustomer(data);
  }

  createCustomerContinue(data: ListeClient) {
    this.clientService.createCustomerContinue(data);
  }
}
