import { Component, OnInit }  from '@angular/core';
import { ClientService }      from '../../services/client.service';
import { ListeClient }        from './liste-clients.interface';
import { Router }             from '@angular/router';
import { HeaderTitleService } from 'src/app/services/header-title.service';



@Component({
  selector   : 'app-liste-clients',
  templateUrl: './liste-clients.component.html',
  styleUrls  : ['./liste-clients.component.css']
})
export class ListeClientsComponent implements OnInit {
  customers: ListeClient[];
  
  constructor( private clientService: ClientService,
               private router       : Router,
               private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Projets terminés')

    this.clientService.getCustomers()
                      .subscribe((data: ListeClient[]) => {
      this.customers = data;
    });
  }
}