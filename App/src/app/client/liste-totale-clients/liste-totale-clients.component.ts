import { Component, OnInit }  from '@angular/core';
import { ClientService }      from '../../services/client.service';
import { ListeClient }        from '../liste-clients/liste-clients.interface';
import { Router }             from '@angular/router';
import { HeaderTitleService } from 'src/app/services/header-title.service';



@Component({
  selector   : 'app-liste-totale-clients',
  templateUrl: './liste-totale-clients.component.html',
  styleUrls  : ['./liste-totale-clients.component.css']
})

export class ListeTotaleClientsComponent implements OnInit {
  customers: ListeClient[];

  constructor( private clientService: ClientService,
               private router: Router,
               private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Liste des clients')

    this.clientService.getCustomers()
                      .subscribe((data: ListeClient[]) => {
      this.customers = data;
    });
  }
}
