import { Component, OnInit }  from '@angular/core';
import { ClientService }      from '../../services/client.service';
import { ListeClient }        from '../liste-clients/liste-clients.interface';
import { HeaderTitleService } from 'src/app/services/header-title.service';


@Component({
  selector   : 'app-modification-client',
  templateUrl: './modification-client.component.html',
  styleUrls  : ['./modification-client.component.css'],
})

export class ModificationClientComponent implements OnInit {
  customers: ListeClient[];
  valeurNgIf  = 0;
  idClient    = null;
  sliceAction = 0;

  constructor( private clientService: ClientService,
               private headerTitleService: HeaderTitleService ) {}

  ngOnInit() {
    this.headerTitleService.setTitle('Modifier un client')

    this.clientService.getCustomers()
                      .subscribe((data: ListeClient[]) => {
      this.customers = data;
    });
  }

  choisirId(idClient) {
    idClient = <HTMLInputElement>document.getElementById("idClientInput");
    idClient = parseInt(idClient.value);

    this.clientService.getCustomer(idClient)
                      .subscribe((data: ListeClient[])=> {
      this.customers = data;
      this.sliceDate(data);
    })

    this.valeurNgIf = 1;
  }

  modifyCustomer(id_client: number, data: ListeClient) {
    data = this.customers[0];
    this.sliceDate(data);
    id_client = Number( this.customers[0].id_client );
    this.clientService.modifyCustomer(id_client, data);
  }

  sliceDate(newDate){
    newDate = this.customers[0].date;
    newDate = newDate.slice(0,10);
    this.customers[0].date = newDate;
    this.sliceAction       = 1;
  }
}
