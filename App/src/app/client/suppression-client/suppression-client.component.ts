import { Component, OnInit }  from '@angular/core';
import { Router }             from "@angular/router";
import { FormBuilder }        from '@angular/forms';
import { ClientService }      from '../../services/client.service';
import { ListeClient }        from '../liste-clients/liste-clients.interface';
import { HeaderTitleService } from 'src/app/services/header-title.service';



@Component({
  selector   : 'app-suppression-client',
  templateUrl: './suppression-client.component.html',
  styleUrls  : ['./suppression-client.component.css']
})

export class SuppressionClientComponent implements OnInit {
  customers: ListeClient[];

  constructor( private clientService     : ClientService,
               private router            : Router,
               private fb                : FormBuilder,
               private headerTitleService: HeaderTitleService ) {}

  ngOnInit() {
    this.headerTitleService.setTitle('Supprimer un client')

    this.clientService.getCustomers()
                      .subscribe((data: ListeClient[]) => {
      this.customers = data;
    });
  }

  deleteCustomer(id_client: number) { 
    this.clientService.deleteCustomer(id_client);
  }
}