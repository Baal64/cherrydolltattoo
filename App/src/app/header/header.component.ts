import { Component, OnInit } from '@angular/core';
import { Title }             from '@angular/platform-browser';
import {AuthService }        from '../services/auth.service';
import { HeaderTitleService } from '../services/header-title.service';

@Component({
  selector   : 'app-header',
  templateUrl: './header.component.html',
  styleUrls  : ['./header.component.css']
})

export class HeaderComponent implements OnInit {
  title = '';
  
  constructor(private headerTitleService: HeaderTitleService, public auth: AuthService) {
    
  }

    ngOnInit() {
      this.headerTitleService.title.subscribe(updatedTitle => {
        this.title = updatedTitle;
      });
    }
}
