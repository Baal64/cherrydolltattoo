import { Component, OnInit }  from '@angular/core';
import { HeaderTitleService } from 'src/app/services/header-title.service';


@Component({
  selector   : 'app-parametres',
  templateUrl: './parametres.component.html',
  styleUrls  : ['./parametres.component.css']
})
export class ParametresComponent implements OnInit {

  constructor( private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Paramètres')
  }
}
