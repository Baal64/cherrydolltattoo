import { Component, OnInit, OnDestroy } from '@angular/core';
import { OnDisableComponentService }    from '../../services/on-disable-component.service';
import { HeaderTitleService }           from 'src/app/services/header-title.service';


@Component({
  selector   : 'app-consentement-mutuel',
  templateUrl: './consentement-mutuel.component.html',
  styleUrls  : ['./consentement-mutuel.component.css']
})
export class ConsentementMutuelComponent implements OnInit {
  public shown = false;

  constructor( private onDisableComponentService: OnDisableComponentService, private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Consentement mutuel')
  }

  // Method from service to disable the checkbox after the click
  // and resize the article
  onDisableComponent() {
    this.onDisableComponentService.onDisableComponent();
  }
}
