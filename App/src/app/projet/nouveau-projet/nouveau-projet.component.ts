import { Component, OnInit } from '@angular/core';
import { HeaderTitleService } from 'src/app/services/header-title.service';

@Component({
  selector   : 'app-nouveau-projet',
  templateUrl: './nouveau-projet.component.html',
  styleUrls  : ['./nouveau-projet.component.css']
})
export class NouveauProjetComponent implements OnInit {

  constructor(private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Nouveau projet')
  }

}
