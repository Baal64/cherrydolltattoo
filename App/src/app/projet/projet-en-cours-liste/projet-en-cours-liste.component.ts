import { Component, OnInit } from '@angular/core';
import { HeaderTitleService } from 'src/app/services/header-title.service';

@Component({
  selector: 'app-projet-en-cours-liste',
  templateUrl: './projet-en-cours-liste.component.html',
  styleUrls: ['./projet-en-cours-liste.component.css']
})
export class ProjetEnCoursListeComponent implements OnInit {

  constructor(private headerTitleService: HeaderTitleService) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Projets en cours')
  }

}
