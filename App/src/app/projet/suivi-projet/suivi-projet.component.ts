import { Component, OnInit }  from '@angular/core';
import { HeaderTitleService } from 'src/app/services/header-title.service';

@Component({
  selector   : 'app-suivi-projet',
  templateUrl: './suivi-projet.component.html',
  styleUrls  : ['./suivi-projet.component.css']
})

export class SuiviProjetComponent implements OnInit {

  constructor( private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Suivi des projets')
  }

}
