import { Component, OnInit }         from '@angular/core';
import { OnDisableComponentService } from '../../services/on-disable-component.service';
import { HeaderTitleService }        from 'src/app/services/header-title.service';


@Component({
  selector   : 'app-tracabilite',
  templateUrl: './tracabilite.component.html',
  styleUrls  : ['./tracabilite.component.css']
})

export class TracabiliteComponent implements OnInit {
  public shown = false;

  constructor( private onDisableComponentService: OnDisableComponentService,
               private headerTitleService       : HeaderTitleService ) {}

  ngOnInit() {
    this.headerTitleService.setTitle('Traçabilité')
  }

  // Method from service to disable the checkbox after the click
  // and resize the article
  onDisableComponent() {
    this.onDisableComponentService.onDisableComponent();
  }
}
