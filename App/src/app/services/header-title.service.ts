import { Injectable }      from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class HeaderTitleService {
    title = new BehaviorSubject('Cherry Doll Tattoo');
  
    setTitle(title: string) {
      this.title.next(title);
    }
  }
