import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})

export class OnDisableComponentService {
  constructor() { }

  onDisableComponent() {
      document.getElementById('disableCssColor').classList.add('disabled');
      document.getElementsByTagName('article')[0].classList.add('NewHeigth');
      document.getElementById('check').setAttribute('disabled', '');
    }
}
