import { Component, OnInit }  from '@angular/core';
import { HeaderTitleService } from 'src/app/services/header-title.service';


@Component({
  selector   : 'app-stocks',
  templateUrl: './stocks.component.html',
  styleUrls  : ['./stocks.component.css']
})
export class StocksComponent implements OnInit {

  constructor( private headerTitleService: HeaderTitleService ) { }

  ngOnInit() {
    this.headerTitleService.setTitle('Stock')

  }

}
